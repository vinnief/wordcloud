a thesis, integrated paper or final project can be an academic research paper or a 
business plan or consultancy project. The business plan and consultancy project are actually
an academic paper with interest in consulting and business planning. As such, they are a business research project and need to show introspection, critical thinking. Hence, not only outcomes but mostly the quality and depth of the description of the academic methods used to arrive at conclusions is going to be evaluated. 

quantitative data and analysis can use primary data such as questionnaires, or secondary data such as organizational documents. The analysis might use t-tests, ANOVA, linear regression or logistic regression, or even SEM models, maybe with SmartPLS SEM analysis.

Qualitative data analysis is more likely to use focus groups or interviews and the analysis consists of transcripts of the inteviews, and intrinsic and extrinsic coding of the interviews transcripts. In consultancy papers, and even more in acadademic thesis, there is a focus on one type of research, hence one can talk of a research method. academic papers use a conceptual model which is based on a literature review. All three types use literature review to know the "state of the art", but literature review for consultancy reports and business plans can be difficult to obtain, as the previous thesis are quite often confidential. If you want your final project to be confidential, ask for an NDA and put a claim on the first page of the thesis. Don't forget a table of contents, list of figures, list of table s, and table of abbreviations. 

The bibliography is required to have APA-style references as usual. After the bibliography, you can add appendices with the analysis that is too detailed to include in the main part of the text. e.g methods that need detailing, transcripts, coding table, regression outputs, etc. 

The most difficult part is the analysis and careful description of the literature, methods, and research project setup. The easiest is the actual analysis, if the project has been set up properly. 
